package app;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.runner.RunWith;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MessageTests {

    @Autowired
    private TestRestTemplate restTemplate;

    private String BASE_URL = "/flash-message/";

    @Test
    public void getListMessage() {
        ResponseEntity<String> response = this.restTemplate.getForEntity(BASE_URL, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = this.restTemplate.getForEntity(BASE_URL + "?q=test", String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = this.restTemplate.getForEntity(BASE_URL + "pageable", String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = this.restTemplate.getForEntity(BASE_URL + "pageable?q=test", String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void getUnknowMessage() {
        ResponseEntity<String> response = this.restTemplate.getForEntity(BASE_URL + "31", String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    @Test
    public void insertEmptyMessage() {
        Message message = new Message();

        ResponseEntity<String> response = this.restTemplate.postForEntity(BASE_URL, message, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_ACCEPTABLE));
    }

    @Test
    public void insertMessage() {
        Message message = new Message();
        message.setContent("test");

        ResponseEntity<String> response = this.restTemplate.postForEntity(BASE_URL, message, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

        String location = response.getHeaders().getLocation().toString();

        Matcher matcher = Pattern.compile("/flash-message/(\\d+)$").matcher(location);
        assertThat("Get the message location", matcher.find());

        String id = matcher.group(1);
        System.out.println("Location : " + BASE_URL + id);

        response = this.restTemplate.getForEntity(BASE_URL, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), equalTo("[{\"id\":" + id + ",\"content\":\"test\",\"active\":true}]"));

        response = this.restTemplate.getForEntity(BASE_URL + "?q=toto", String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), equalTo("[]"));

        response = this.restTemplate.getForEntity(BASE_URL + id, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), equalTo("{\"id\":" + id + ",\"content\":\"test\",\"active\":true}"));

        this.restTemplate.delete(BASE_URL + id);
        response = this.restTemplate.getForEntity(BASE_URL + id, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));

        response = this.restTemplate.postForEntity(BASE_URL, message, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

        this.restTemplate.delete(BASE_URL);
        response = this.restTemplate.getForEntity(BASE_URL, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), equalTo("[]"));
    }

}
