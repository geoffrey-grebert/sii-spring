package app;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @ApiModelProperty(notes="The generated message ID", readOnly=true, hidden=true)
    private Long id;

    @Column(nullable=false)
    @ApiModelProperty(notes="The message", required=true)
    private String content;

    @Column(nullable=false)
    private boolean isActive = true;

    protected Message() {}

    public Message(String content) {
        this.content = content;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the message
     */
    public String getContent() {
        return content;
    }

    /**
     * @return the isActive
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * @param message the message to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return String.format("Message[id=%d, message=%s]", id, content);
    }

}
