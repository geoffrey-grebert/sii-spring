package app;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface MessageRepository extends PagingAndSortingRepository<Message, Long> {

    @Query ("SELECT m FROM Message m WHERE lower(m.content) LIKE lower(CONCAT('%',:message,'%'))")
    Page<Message> findAllByMessage(@Param("message") String message, Pageable pageable);

    @Query("SELECT m FROM Message m WHERE lower(m.content) LIKE lower(CONCAT('%',:message,'%'))")
    Iterable<Message> findAllByMessage(@Param("message") String message);

}
