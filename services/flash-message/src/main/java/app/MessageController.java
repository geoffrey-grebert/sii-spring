package app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import static net.logstash.logback.marker.Markers.append;

@RestController
@RequestMapping("/flash-message")
class MessageController {

    @Autowired
    private MessageRepository repository;

    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

    @ApiOperation(value = "List all messages")
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Message> all(
        @ApiParam( name = "q", required = false, value = "Search filter" )
        @RequestParam( name = "q", required = false) String filter
    ) {
        if (filter == null) {
            return repository.findAll();
        }

        return repository.findAllByMessage(filter);
    }

    @ApiOperation(value = "List all messages with pageable")
    @ApiImplicitParams({ // @see https://github.com/springfox/springfox/issues/755
        @ApiImplicitParam(
            name = "page",
            dataType = "int",
            paramType = "query",
            value = "Results page you want to retrieve (0..N)"
        ),
        @ApiImplicitParam(
            name = "size",
            dataType = "int",
            paramType = "query",
            defaultValue = "20",
            value = "Number of records per page."
        ),
        @ApiImplicitParam(
            name = "sort",
            allowMultiple = true,
            dataType = "String",
            paramType = "query",
            value = "Sorting criteria in the format: property(,asc|desc). " +
                    "Default sort order is ascending. " +
                    "Multiple sort criteria are supported."
        )
    })
    @RequestMapping(path = "/pageable", method = RequestMethod.GET)
    public Page<Message> list(
        Pageable pageable,
        @ApiParam( name = "q", required = false, value = "Search filter" )
        @RequestParam( name = "q", required = false) String filter
    ) {
        if (filter == null) {
            return repository.findAll(pageable);
        }

        return repository.findAllByMessage(filter, pageable);
    }

    @ApiOperation(value="Create new message")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> create(@RequestBody Message message, UriComponentsBuilder ucBuilder) {
        logger.info("Creating new message", append("flash-message", message));

        try {
            repository.save(message);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/flash-message/{id}").buildAndExpand(message.getId()).toUri());

            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
        catch(Exception e) {
            logger.error("Unable to create message.", e);

            return new ResponseEntity<>("Unable to create message.", HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @ApiOperation(value = "Delete all messages")
    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Message> empty() {
        logger.info("Deleting all messages");

        try {
            repository.deleteAll();

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
            logger.error("Unable to delete all messages.", e);

            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @ApiOperation(value = "Get a specific message")
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Message> get(@PathVariable("id") long id) {
        Message message = repository.findOne(id);

        if (message == null) {
            logger.error("Message not found.", append("id", id));

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete a specific message")
    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Message> delete(@PathVariable("id") long id) {
        logger.info("Deleting message.", append("id", id));

        Message message = repository.findOne(id);

        if (message == null) {
            logger.error("Unable to delete. Message not found.", append("id", id));

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try {
            repository.delete(message);

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
            logger.error("Unable to delete message.", e);

            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

}
