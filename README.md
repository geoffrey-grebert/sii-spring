# Services list

| Name              | Port |
|-------------------|------|
| elasticsearch | 9200 |
| logstash | 4560 |
| kibana | 5601 |
| [eureka](/services/eureka/README.md) | 8761 |
| [zuul](/services/zull/README.md) | 8079 |
| [turbine](/services/turbine/README.md) | 8989 |
| [hystrix-dashboard](/services/hystrix-dashboard/README.md) | 7979 |
| [flash-message](/services/flash-message/README.md) | 8081 |


# TODO

 - documentation for each services
 - Build test for file encoding
 - Build test for branch and naming convention
 - add jacoco for coverage
 - frontend service
 - tests e2e
 - template for Merge Requests
 - auto generate usage documentation with `GitLab page`
 - Add auto deploy script for kubernetes
