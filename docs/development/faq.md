# FAQ

Some Frequently Asked Questions.


## I have no log in the console

By default, we use the `stash Appender` wich send logs with `GELF` to logstash.

To send logs on `STDERR` you have to set the environment variable `SPRING_PROFILES_ACTIVE` to `dev`.

Example in bash :

```bash
export SPRING_PROFILES_ACTIVE=dev
java -jar service/$MY_SERVICE/target/$MY_SERVICE-dev.jar
```


## Job `trigger keep-artifacts` failed

Error message :
```
urllib.error.HTTPError: HTTP Error 401: Unauthorized
ERROR: Job failed: exit code 1
```

You have to [configure your TOKEN for CI]()

