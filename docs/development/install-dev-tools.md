# Install and configure dev environment

## Software stack

* Java
* Python 3.6+
* maven 3.3+
* Git 2.8.4+

__Install JAVA__
- For Debian/Ubuntu
```bash
apt-get install default-jdk
```

__Install maven__
- For Debian/Ubuntu
```bash
apt-get install maven
```

__Install Python__
- For Debian/Ubuntu
```bash
apt-get install -y python3-pip
```

- For [Windows](https://www.python.org/downloads/)

  In the installation, don't forget to put python to your `PATH`


__Install GIT__
- For Debian/Ubuntu
```bash
apt-get install git
```
- For [Windows](https://git-scm.com/download/win)

  In the installation process, don't forget to put git to your `PATH`


__Install docker__

docker engine :

- Linux
  - [Ubuntu](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/)
  - [Debian](https://docs.docker.com/engine/installation/linux/docker-ce/debian/)
  - [Fedora](https://docs.docker.com/engine/installation/linux/docker-ce/fedora/)
  - [CentOS](https://docs.docker.com/engine/installation/linux/docker-ce/centos/)
- [Windows](https://docs.docker.com/docker-for-windows/install/)
- [Mac](https://docs.docker.com/docker-for-mac/install/)

docker-compose : https://docs.docker.com/compose/install/


## Frameworks

* [Spring Boot](https://projects.spring.io/spring-boot/)
* [Spring Cloud](http://projects.spring.io/spring-cloud/)

