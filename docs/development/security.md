# Security

## Check dependency vulnerabilities

The OWASP Top 10 2013 contains a new entry:
[A9 - Using Components with Known Vulnerabilities](https://www.owasp.org/index.php/Top_10_2013-A9-Using_Components_with_Known_Vulnerabilities).
Dependency-check can currently be used to scan applications (and their dependent libraries) to identify any known vulnerable components.


__Generate report__

```bash
./mvnw -fn dependency-check:aggregate
```
The output file is `target/dependency-check-report.html`

[Latest auto generated report for master branch](/../-/jobs/artifacts/master/raw/target/dependency-check-report.html?job=security report dependency-check)


__Important notes__

* Dependency-check automatically updates itself using the [NVD Data Feeds](https://nvd.nist.gov/vuln/data-feeds) hosted by NIST.

* The initial download of the data may take ten minutes or more, if you run the tool at least once every seven days only a small XML file needs to be downloaded to keep the local copy of the data current.

