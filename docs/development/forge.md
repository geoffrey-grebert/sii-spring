# Configure your forge


## Protect your branches

Go to [Repository settings](/../settings/repository) and expand the `Protected Branches` section.

Rules for protected branches :

 - Allowed to merge : **Masters**
 - Allowed to push : **No one**

Protected branches :

 - `master`
 - `preprod`

Protected wildcard :

 - `^[\d\.]+$`


## Protect your tags

Go to [Repository settings](/../settings/repository) and expand the `Protected Tags` section.

Protected tags wildcards :

 - `*`


## CI TOKEN

ROLE : **`MASTER`**

You have to configure a specific TOKEN for pipelines to acces the GitLab API.

1. Create a personal Token in your [User Settings](https://gitlab.com/profile/personal_access_tokens)
with no expiration date and with only `api` scope.
![screenshot create token](img/forge-ci-token-1.png)


2. Paste the new generated Token in new [CI Secret variable](/../settings/ci_cd) called `API_TOKEN`. For security reason, check `Protected` to limit access to the variable.
![screenshot create var](img/forge-ci-token-2.png)

