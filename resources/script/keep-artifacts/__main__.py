#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    keep-artifacts
    ==============

    This script is used to save indefinitely the artifacts of the current pipeline.

    GitLab CI environment variables used :
        - API_TOKEN
        - CI_PIPELINE_ID
        - CI_PROJECT_ID
        - CI_PROJECT_PATH
        - CI_PROJECT_URL
"""

import json
import os
import urllib.request
import sys

GITLAB_URL = os.environ.get('CI_PROJECT_URL').replace(os.environ.get('CI_PROJECT_PATH'), '')
GITLAB_API = GITLAB_URL + 'api/v4/'
TOKEN = os.environ.get('API_TOKEN')
PIPELINE_ID = os.environ.get('CI_PIPELINE_ID')
PROJECT_ID = os.environ.get('CI_PROJECT_ID')

def main():
    """
        Main function
    """

    for job in getjobs():
        if 'artifacts_file' in job:
            keep(job)
            print('Job ' + job['name'] + ' saved')

def getjobs():
    """
        Return all jobs of the current pipeline
    """

    url = GITLAB_API + 'projects/' + PROJECT_ID + '/pipelines/' + PIPELINE_ID + '/jobs'

    req = urllib.request.Request(url, headers={'PRIVATE-TOKEN': TOKEN})
    res = urllib.request.urlopen(req).read()

    return json.loads(res.decode('utf-8'))

def keep(job):
    """
        Keep artifact of a specific job

        :param job: Job informations
        :type job: dict
    """

    url = GITLAB_API + 'projects/' + PROJECT_ID + '/jobs/' + str(job['id']) + '/artifacts/keep'

    req = urllib.request.Request(url, headers={'PRIVATE-TOKEN': TOKEN}, method='POST')
    urllib.request.urlopen(req)

if __name__ == "__main__":
    main()
