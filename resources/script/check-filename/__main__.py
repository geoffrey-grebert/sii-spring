#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    check-filename
    ==============

    This script is used to validate file convention:
        - filename
        - EOL unix
        - encoding
        - final new line
"""

import os
import re
import sys
import subprocess

pattern_dir = re.compile(r'^[\w\.\-\/]+$')
pattern_file = re.compile(r'^[\w\.\-]+$')
pattern_info = re.compile(r':\s([^;]+);\s+charset=(.+)')

valid_charsets = (
    'us-ascii',
    'utf-8',
    'binary',
)

dos_ext = (
    '.cmd',
)

def main():
    """
        Main function
    """

    try:
        for search_results in os.walk('./'):
            directory = search_results[0]

            # skip git directory
            if directory == "./.git" or "./.git" + os.sep in directory:
                continue

            testDirname(directory)

            for filename in search_results[2]:
                file = os.path.join(directory, filename)

                testFilename(filename)

                info = getFileInfo(file)

                testCharset(info["charset"], file)
                testEOL(info, file)
                testFinalNewLine(info, file)

                print("[OK] {}".format(file))
    except Exception as e:
        print("[ERROR] {} for {}".format(e.args[0], e.args[1]))
        sys.exit(1)

def testDirname(directory):
    if sys.platform == 'win32':
        directory = directory.replace('\\', '/')

    if not pattern_dir.match(directory):
        raise Exception('Invalid directory name', directory)

def testFilename(file):
    if not pattern_file.match(file):
        raise Exception('Invalid file name', file)

def testCharset(charset, file):
    if charset not in valid_charsets:
        raise Exception('Invalid encoding', file)

def testEOL(info, file):
    if info['charset'] == 'binary':
        return

    with open(file, 'rb') as f:
        for line in f.readlines():
            if line[-2:] == b'\r\n':
                if not isTrackedFile(file) or isDosFile(file):
                    return

                raise Exception('file in dos format', file)

def testFinalNewLine(info, file):
    if info['charset'] == 'binary':
        return

    with open(file, 'rb') as f:
        line = f.readlines()[-1]
        if line[-1:] != b'\n':
            if not isTrackedFile(file):
                return
            raise Exception('no final new line', file)

def getFileInfo(file):
    process = subprocess.Popen(['file', '-i', file], stdout=subprocess.PIPE)

    stdout = process.communicate()[0].decode(sys.getdefaultencoding())

    if process.returncode:
        raise Exception('Cannot get informations', file)

    result = pattern_info.findall(stdout)

    return {
        'mime': result[0][0],
        'charset': result[0][1],
    }

def isTrackedFile(file):
    process = subprocess.Popen(
        ['git', 'ls-files', '--error-unmatch', file],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    process.communicate()

    if process.returncode:
        return False

    return True

def isDosFile(file):
    if os.path.splitext(file)[1] in dos_ext:
        return True

    return False

if __name__ == "__main__":
    main()
