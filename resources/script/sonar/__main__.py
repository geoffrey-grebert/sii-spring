#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    sonar
    ==============

    GitLab CI environment variables used :
        - API_TOKEN
        - CI_COMMIT_SHA
        - CI_PROJECT_ID
        - CI_PROJECT_PATH
        - CI_PROJECT_URL
        - SONAR_REPORT_FILE
        - SONAR_ISSUE_ONLY_NEW
"""

import json
import os
import urllib.request
import string

GITLAB_URL = os.environ.get('CI_PROJECT_URL').replace(os.environ.get('CI_PROJECT_PATH'), '')

GITLAB_API = GITLAB_URL + 'api/v4/'

TOKEN = os.environ.get('API_TOKEN')

PROJECT_ID = os.environ.get('CI_PROJECT_ID')

COMMIT_SHA = os.environ.get('CI_COMMIT_SHA')

REPORT_FILE = 'target/sonar/' + os.environ.get('SONAR_REPORT_FILE')

ONLY_NEW_ISSUE = True if os.environ.get('SONAR_ISSUE_ONLY_NEW') else False

TEMPLATE = string.Template(
"""${message}
- Severity : __${severity}__
- Rule : [${rule}](https://sonarcloud.io/organizations/default/rules#rule_key=${rule})
- File : [${component}](${file}#L${line})"""
)

def main():
    """
        Main function
    """

    # read sonar job report
    with open(REPORT_FILE) as f:
        report = json.load(f)

    for issue in report["issues"]:

        # filter is want only new issue
        if ONLY_NEW_ISSUE and issue["isNew"]:
            continue

        # find file path from root directory
        file = issue["component"]
        for component in report["components"]:
            if "path" in component:
                file = file.replace(component["key"] + ":", component["path"] + "/")
        issue["file"] = file

        # generate note
        issue["note"] = TEMPLATE.substitute(issue)

        # send to gitlab
        url = GITLAB_API + 'projects/' + PROJECT_ID + '/repository/commits/' + COMMIT_SHA + '/comments'

        print("----------")
        print(issue["note"])

        # send message to GitLab
        req = urllib.request.Request(url,
            headers = {'PRIVATE-TOKEN': TOKEN},
            method = 'POST',
            data = urllib.parse.urlencode({
                'path': issue["file"],
                'line': issue["line"],
                'line_type': "new",
                'note': issue["note"]
            }).encode("utf-8")
        )
        result = urllib.request.urlopen(req)

        print(json.dumps(json.loads(result.read().decode('utf-8')), sort_keys = True, indent = 4))

if __name__ == "__main__":
    main()
