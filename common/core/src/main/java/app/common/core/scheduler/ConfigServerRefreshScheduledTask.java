package app.common.core.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.endpoint.RefreshEndpoint;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ConfigServerRefreshScheduledTask {

    @Autowired
    private RefreshEndpoint refreshEndpoint;

    @Scheduled(
        fixedRateString = "${config.refresh.interval:20000}",
        initialDelayString = "${config.refresh.delay:60000}"
    )
    public void refreshConfigValues() {
        refreshEndpoint.invoke();
    }

}
